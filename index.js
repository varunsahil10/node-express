const express = require('express');
const http = require('http');
const hostname = 'localhost';
const port = 3000;
const bodyParser = require('body-parser');
const morgan = require('morgan');
const app = express();
app.use(morgan('dev'));

app.use(bodyParser.json());

app.all('/dishes',(req,res,next)=>{
	res.statusCode = 200;
	res.setHeader('Content-Type','text/plain');
	next();
});
app.get('/dishes',(req,res,next)=>{
	res.write('ye write wali statement h\n');
	res.end('Send all the dishes!!!');
	
});
app.post('/dishes',(req,res,next)=>{
	res.end('Added the dish: '+req.body.name+'\n With the description as: '+req.body.description);
});
app.put('/dishes',(req,res,next)=>{
	res.end('The updation in a list can be done directly... Kindly specifically visit the dish to update.');

});
app.delete('dishes',(req,res,next)=>{
	res.end('All the dishes will be deleted');
});
app.get('/dishes/:dishId', (req,res,next) => {
    res.end('Will send details of the dish: ' + req.params.dishId +' to you!');
});

app.post('/dishes/:dishId', (req, res, next) => {
  res.statusCode = 403;
  res.end('POST operation not supported on /dishes/'+ req.params.dishId);
});

app.put('/dishes/:dishId', (req, res, next) => {
  res.write('Updating the dish: ' + req.params.dishId + '\n');
  res.end('Will update the dish: ' + req.body.name + 
        ' with details: ' + req.body.description);
});

app.delete('/dishes/:dishId', (req, res, next) => {
    res.end('Deleting dish: ' + req.params.dishId);
});

app.use(express.static(__dirname+'/public'));
app.use((req,res,next)=>{
	
	res.statusCode = 200;
	res.setHeader('Content-Type', 'text/html');
  	res.end('<html><body><h1>This is an Express Server</h1></body></html>');


});

const server = http.createServer(app);

server.listen(port,hostname,()=>{
	console.log(`server running at http://${hostname}:${port}`);
});